import secrets
import os.path
import sys

print('==========================')
print('SyncdIt Self-Hosting Setup')
print('==========================')
print()

if os.path.isdir('.env'):
	print('.env is a directory. Please remove to continue. Exiting...')
	sys.exit(1)
if os.path.isfile('.env'):
	if input('.env is a file. Would you like to overwrite? [y/N] ').lower() != 'y':
		print('Exiting...')
		sys.exit(1)

cid = input('Spotify Application Client ID: ')
c_secret = input('Spotify Client Secret (will echo): ')
if input('Would you like to automatically generate the Flask application secret key? [Y/n] ').lower() == 'n':
	app_secret = input('Application Secret Key: ')
else:
	app_secret = secrets.token_hex(32)
redirect_uri = input('Application Redirect URI: ')

with open('.env', 'w') as f:
	f.write(f"""\
cid={cid}
csecret={c_secret}
app_secret_key={app_secret}
redirect_uri={redirect_uri}
""")