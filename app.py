from flask import Flask, redirect, request, session, render_template, url_for

import spotipy
from spotipy.oauth2 import SpotifyOAuth
from spotipy.cache_handler import CacheHandler
from dotenv import load_dotenv

from functools import wraps
import os
import traceback
import time

load_dotenv()

app = Flask(__name__, static_url_path='', static_folder='static')

app.config['SECRET_KEY'] = os.getenv('app_secret_key')

class FlaskSessionCacheHandler(CacheHandler):
	"""
	A cache handler that stores the token info in the session framework
	provided by flask.
	"""

	def __init__(self, session):
		self.session = session

	def get_cached_token(self):
		token_info = None
		try:
			token_info = self.session["token_info"]
		except KeyError:
			print("Token not found in the session")

		return token_info

	def save_token_to_cache(self, token_info):
		try:
			self.session["token_info"] = token_info
		except Exception as e:
			print("Error saving token to cache: " + str(e))

def parse_playlist_data(current_user, results, to_append_list, sp):
	for item in results['items']:
		if current_user['id'] == item['owner']['id']:
			try:
				image_url = item['images'][0]['url']
			except IndexError:
				image_url = "/img/musicnote.png"

			to_append_list.append({
				'id': item['id'],
				'image': image_url,
				'name': item['name'],
				'description': item['description']
			})

	if results['next']:
		results = sp.next(results)
		parse_playlist_data(current_user, results, to_append_list, sp)

def get_song_ids(results, to_append_list, sp):
	for item in results['items']:
		to_append_list.append(item['track']['uri'])

	if results['next']:
		results = sp.next(results)
		get_song_ids(results, to_append_list, sp)

def chunks(lst, n):
	"""Yield successive n-sized chunks from lst."""
	for i in range(0, len(lst), n):
		yield lst[i:i + n]

def delete_cache_file():
	try:
		os.remove('.cache')
	except FileNotFoundError:
		pass

def requires_auth(func):
	@wraps(func)
	def wrapper(*args, **kwargs):
		token_info = session.get('spotify_session', {})

		if not session.get('spotify_session', False):
			# token invalid
			return redirect('/auth')

		now = int(time.time())
		is_token_expired = session.get('spotify_session').get('expires_at') - now < 60

		# refresh token
		if is_token_expired:
			sp_oauth=SpotifyOAuth(
				scope='user-library-read playlist-modify-public',
				client_id=os.getenv('cid'),
				client_secret=os.getenv('csecret'),
				redirect_uri=os.getenv('redirect_uri'),
				cache_handler=FlaskSessionCacheHandler(session=session)
			)
			token_info = sp_oauth.refresh_access_token(session.get('spotify_session').get('refresh_token'))

		# token valid
		session['spotify_session'] = token_info
		return func(*args, **kwargs)
	return wrapper

@app.route('/')
@requires_auth
def index():
	if 'playlist-id' not in request.cookies:
		# playlist isn't selected
		return redirect(url_for('choose_playlist'))

	sp = spotipy.Spotify(auth=session.get('spotify_session').get('access_token'))
	playlist = sp.playlist(request.cookies.get('playlist-id'))
	return render_template('syncsongs.html', playlist=playlist)

@app.route('/auth/')
def auth():
	delete_cache_file()
	
	sp_oauth=SpotifyOAuth(
		scope='user-library-read playlist-modify-public playlist-modify-private',
		client_id=os.getenv('cid'),
		client_secret=os.getenv('csecret'),
		redirect_uri=os.getenv('redirect_uri'),
		cache_handler=FlaskSessionCacheHandler(session=session)
	)

	auth_url = sp_oauth.get_authorize_url()
	return redirect(auth_url)


@app.route('/auth_callback')
def auth_callback():
	sp_oauth=SpotifyOAuth(
		scope='user-library-read playlist-modify-public playlist-modify-private',
		client_id=os.getenv('cid'),
		client_secret=os.getenv('csecret'),
		redirect_uri=os.getenv('redirect_uri'),
		cache_handler=FlaskSessionCacheHandler(session=session)
	)

	code = request.args.get('code')
	token_info = sp_oauth.get_access_token(code, as_dict=True)

	session['spotify_session'] = token_info

	return redirect(url_for('index'))

@app.route('/chooseplaylist/')
@requires_auth
def choose_playlist():
	sp = spotipy.Spotify(auth=session.get('spotify_session').get('access_token'))
	playlists = []
	results = sp.current_user_playlists()

	parse_playlist_data(sp.current_user(), results, playlists, sp)

	return render_template('chooseplaylist.html', playlists=playlists)

@app.route('/syncsongs/', methods=['GET'])
def syncsongs():
	access_token = session.get('spotify_session', None)
	if not access_token:
		return 'reauth'
	
	# test if token is session is valid
	try:
		sp = spotipy.Spotify(auth=access_token.get('access_token'))
		sp.current_user()
	except spotipy.exceptions.SpotifyException:
		return 'reauth'

	if 'playlist-id' not in request.cookies:
		return 'chooseplaylist'

	try:
		playlist_id = request.cookies.get('playlist-id')

		# remove all items from playlist
		playlist_items = []
		get_song_ids(sp.playlist_tracks(playlist_id), playlist_items, sp)
		playlist_chunked_list = list(chunks(playlist_items, 100))
		for chunk in playlist_chunked_list:
			sp.playlist_remove_all_occurrences_of_items(playlist_id, chunk)

		# add liked songs to playlist
		results = sp.current_user_saved_tracks()
		id_list = []
		get_song_ids(results, id_list, sp)
		chunked_list = list(chunks(id_list, 100))
		for chunk in chunked_list:
			sp.playlist_add_items(playlist_id, chunk)
		return 'true'
	except Exception as e:
		traceback.print_exc()
		return str(e), 500

@app.route('/logout')
def logout():
	session.clear()
	return 'success'